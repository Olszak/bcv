//PRZYKLEJANIE MENU WORK EXPERIENCE
jQuery("document").ready(function($){
    var nav = $('.work-menu');
    $(window).scroll(function () {
        if ($(this).scrollTop() > 190) {
            nav.addClass("f-nav");
        } else {
            nav.removeClass("f-nav");
        }
    });
 
});

//MENU GŁÓWNE KOMÓRKA 
jQuery("document").ready(function($){
  $(".nav-header").click(function(){
    $("#menu-menu-principal-en").toggle();
  });
});

//MENU BOCZNE WORK
jQuery("document").ready(function ($) {
    $(".menu-left-menu-container ul#menu-left-menu-2 li .sub-menu li").click(function () {
        $("li", this).toggle();
    });
});

//SCROLL DO MENU WORK EXPERIENCE
jQuery(function($) {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 500);
        return false;
      }
    }
  });
});