<?php 
/**
 * Template Name: Plane-Crash
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage antyki

 */
 
 get_header();

?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
		
<?php //*******************************************************************************************************************// ?>		
	
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer.js"></script>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="http://maps.stamen.com/js/tile.stamen.js?v1.3.0"></script>		

<script type="text/javascript"><!--
var map          = null;
var markers      = new Array();
var markers_info = new Array();
var infoWindow   = new google.maps.InfoWindow();
jQuery.fn.scrollView = function () {
return this.each(function () {
jQuery('html, body').animate({
scrollTop: jQuery(this).offset().top
}, 1000);
});
}

function initialize(){

var layer = "toner-lite";
var store_location = new google.maps.LatLng(37.968930, 0.651457);
var map_canvas  = document.getElementById('map');
var map_options = {
center: store_location,
mapTypeControl: false,
streetViewControl: false,
zoom: 2,
scrollwheel: false,      
mapTypeId: layer,
mapTypeControlOptions: {
						mapTypeIds: [layer]
					},
zoomControlOptions: {
style: google.maps.ZoomControlStyle.LARGE
}
};
map = new google.maps.Map(map_canvas, map_options);

map.mapTypes.set(layer, new google.maps.StamenMapType(layer));
var opt = { minZoom: 2, maxZoom: 16 };
map.setOptions(opt);


}


jQuery(document).ready(function() {

initialize();

// var layer = "toner-lite";
// var store_location = new google.maps.LatLng(37.968930, 0.651457);
// var map_canvas  = document.getElementById('map');
// var map_options = {
// center: store_location,
// mapTypeControl: false,
// streetViewControl: false,
// zoom: 2,
// scrollwheel: false,      
// mapTypeId: layer,
// mapTypeControlOptions: {
						// mapTypeIds: [layer]
					// },
// zoomControlOptions: {
// style: google.maps.ZoomControlStyle.LARGE
// }
// };
// map = new google.maps.Map(map_canvas, map_options);

// map.mapTypes.set(layer, new google.maps.StamenMapType(layer));
// var opt = { minZoom: 2, maxZoom: 16 };
// map.setOptions(opt);
// });

});

function showMarkerInfo(id) {

		$("#map-info").hide();
		$("#map-container").show();
		$("#map-close").show();
	
		$(".kreska").hide();
		
initialize();
var opt_options = {
	minimumClusterSize: 5
	};
	
	var markerCluster = new MarkerClusterer(map, markers, opt_options);
	markerCluster.redraw();

infoWindow.close();
infoWindow.setContent('<div id="info">'+markers_info[id]+'</div>');
infoWindow.open(map, markers[id]);
map.setZoom(8);
map.setCenter(markers[id].position);
jQuery('#map').scrollView();
}

jQuery(function($) {
	$(document).ready(function(){
	
	//$("#map-container").hide();

	$("#map-close").click(function(){
		$("#map-container").hide();
		$("#map-close").hide();
		$("html").css("overflow", "scroll");
	});
	
	$("li.crash").click(function(){
	
		setTimeout(function() {
	
		if ($(window).width() > 800){
		$(this).find(".kreska").show();
		}
		else{
		$("html").css("overflow", "hidden");
		}
		
		var mapcia = $(this).find(".kreska");
		var mappos = mapcia.offset();
		console.log(mappos);
		
		var lindol = $("html").height(); 
		var hejf = mappos.top;
		
		var hcontent = $(".entry-content").offset();
		var hctop = hcontent.top;
		
		var czynnik = (lindol-hctop)-(hejf-hctop);
		
		var percent = 1-(czynnik/(lindol-hctop));
		
		var econtent = $(".entry-content").height();
		var hcontent = $(".entry-content").offset();
	
		var kreskatop = mappos.top-hcontent.top;
	
		var mpct = $("#map-container").height();
		console.log(percent);
		
		$("#map-container").css("top" , kreskatop-(mpct*percent)-55);
		
		}, 100);
		
		
	});
	

	
	});
});
--></script>

<?php //*******************************************************************************************************************// 

global $licznik;
$licznik = 0;

function show_plane($year){
									global $licznik;
																// WP_Query arguments
									$args = array (
										'post_type'              => 'plane',
										'meta_key'				 => 'year',
										'meta_value'			 => $year
									);

									// The Query
									$query = new WP_Query( $args );

									// The Loop
									if ( $query->have_posts() ) {
									echo "<ul>";
											echo '<div class="bcvyear">'.$year.'</div>';
										while ( $query->have_posts() ) {
										
											$query->the_post();
											$bcvpos = get_field("position");
											$bcvyear = get_field("year");
											
											echo "<li class='crash' onclick='showMarkerInfo(". $licznik .")'>";
											echo "<div class='kreska'></div>";
											
											echo "<span>";
											
											echo get_the_title();
											echo ", ";
											echo get_the_content();
											echo " (";
											echo $bcvpos["address"];
											echo ", ";
											echo $bcvyear;
											echo ")</span>";
											echo "</li>";
											
											/*icon: '".get_template_directory_uri()."/images/mapmarker.png' // null = default icon*/
											
											echo "<script type='text/javascript'>
											markers[".$licznik."] = new google.maps.Marker({
											position: new google.maps.LatLng(".$bcvpos["lat"].", ".$bcvpos["lng"]."),
											map: map
											});
											markers_info[".$licznik."] = '".get_the_title().", ".get_the_content()."';
											google.maps.event.addListener(markers[".$licznik."], 'click', function() {
											showMarkerInfo(".$licznik.");
											});
											</script>
											";
											$licznik++;
											
										
										}
										echo "</ul>";
									} else {
										// no posts found
									}
									
									// Restore original Post Data
									wp_reset_postdata();


}

?>	
		
		
			<section class="plane-crash">
			
				<?php /* The loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
				
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header class="entry-header">
							<h1 class="entry-title"><?php the_title(); ?></h1>
							<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
							<div class="entry-thumbnail">
								<?php the_post_thumbnail(); ?>
							</div>
							<?php endif; ?>
						</header><!-- .entry-header -->

						<div class="entry-content">
							
							<div id="map-info">
							Click on crash title to show map with position.
							</div>
							
							<div id="map-close">
									X
							</div>
							
							<div id="map-container">
								<div id="map">
								</div>
							</div>

							<div> 
									<?php //the content here ?>
							
									<?php
									for ($i = 1900; $i < 2100; $i++){
										
										show_plane($i);
										
									}
									?>
							 </div>
						
						</div><!-- .entry-content -->

						<footer class="entry-meta">
							<?php edit_post_link( __( 'Edit', 'antyki' ), '<span class="edit-link">', '</span>' ); ?>
						</footer><!-- .entry-meta -->
					</article><!-- #post -->

				<?php endwhile; ?>
				
			</section>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
