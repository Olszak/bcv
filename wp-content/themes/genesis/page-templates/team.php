<?php 
/**
 * Template Name: Team
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage antyki

 */
 
 get_header();

?>


		<div id="primary" class="content-area">
			<nav class="left-menu">
		
				<?php wp_nav_menu(array( 'theme_location' => 'third-menu')); ?>
	
			</nav>
			
			<nav class="left-menu-toggle">
			<?php wp_nav_menu(array( 'theme_location' => 'third-menu')); ?>
			</nav>
			
		<div id="content" class="site-content" role="main">
		
			<section class="team">
			
				<?php /* The loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
				
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header class="entry-header">
							<h1 class="entry-title"><?php the_title(); ?></h1>
							<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
							<div class="entry-thumbnail">
								<?php the_post_thumbnail(); ?>
							</div>
							<?php endif; ?>
						</header><!-- .entry-header -->

						<div class="entry-content">
							<?php the_content(); ?>
							<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'antyki' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
						</div><!-- .entry-content -->

						<footer class="entry-meta">
							<?php edit_post_link( __( 'Edit', 'antyki' ), '<span class="edit-link">', '</span>' ); ?>
						</footer><!-- .entry-meta -->
					</article><!-- #post -->

				<?php endwhile; ?>
				
			</section>
			
	

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
